# 1-alpine version 1 and alpine which is the most lightweight version of nginx
FROM nginxinc/nginx-unprivileged:1-alpine

LABEL maintainer="vass@mailer.com"

# Needed to customize the nginx instance running to our needs
COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9001

# Some changes we need to be root user for, only for these lines
USER root

# Create the static volume directory and set the right permissions needed for nginx
# 755: Owner can do all, group and public can excecute and read (not write)
RUN mkdir -p /vol/static
RUN chmod 755 /vol/static

# Creates the file that will be updated later on
# But this allows to already set the right permissions to it user and group nginx
# So when the entrypoint script that creates a conf from the template file, nginx user can do this
RUN touch /etc/nginx/conf.d/default.conf 
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf


# This also needs to be excecutable to run it
COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# Root user operations are finished, let's switch back to the less priviliged nginx user
USER nginx

# Define the command to run when the dockerfile has ran
CMD ["/entrypoint.sh"]


