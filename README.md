# Recipe App API Proxy


NGINX proxy app for our recipe app API

## Usage

### Environment Variables

* `LISTEN_PORT`: Port to listen on (default: `8000`)
* `APP_HOST`: Hostname of the app to forward requests to (default: `app`)
  - Host that the proxy forwards requests to
* `APP_PORT`: Port that the app forward requests to (default: `9001`)