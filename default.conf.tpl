server {
  listen ${LISTEN_PORT};

  # NOTE: nginx will handle them in the order we define the location blocks
  location /static {
    
    # This will route the traffic to a directory/volume on the proxy server
    alias /vol/static;

  }

  # NOTE: If this was put first, then still static requests will be handled by django
  location / {

    # Will pass requests to the uwsgi service
    uwsgi_pass            ${APP_HOST}:${APP_PORT};

    # Required by nginx for it to work usqgi, more found here:
    # - Example: https://gitlab.com/LondonAppDev/recipe-app-api-devops-course-material/-/snippets/1944796
    # - Docs: https://uwsgi-docs.readthedocs.io/en/latest/Nginx.html#what-is-the-uwsgi-params-file
    include               /etc/nginx/uwsgi_params;

    # The course application will need to upload images, so a bit larger then default
    client_max_body_size  10M;
  }

}