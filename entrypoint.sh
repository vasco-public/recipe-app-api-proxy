#!/bin/sh

# During the excecution of this script, return a failure and stops the script
# Which is really helpfull for debugging. So make sure it fails
set -e

# envirnment substitute, we pass in the template file we want to substitue the vars for
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf


# This will tell nginx to run withouth a daemon turned on
# By default nginx runs as a background daemon service, we need foreground
# It's recommanded that docker containers with the priori app in the foreground
nginx -g 'daemon off;'


